'use strict';
//Simple javascript example for the AustinIoT MQTT QuickStart
//Find more info at the BitBucket Repo
//https://bitbucket.org/uCHobby/web_mqtt_quickstart
//David Fowler

/*global Messaging */  //Tell linter that we have a global named Messaging. 

//MQTT Broker 
var mqttAddress = "austiniot.com";  //**Change This***  Host where your MQTT borker lives
var mqttClientID = "uCHobbyWeb";    //**Change This***  Must be unique across all connections, one id per.
var mqttPort = 8883;                //Port for Websocet connections to the broker
var mqttConnection;                 //The MQTT connection object

var mqttConnectionOptions = {       //Some connection details for the MQTT broker
    onSuccess: onMQTTConnect,       //Connect event handler
	onFailure: onMQTTFail           //Fail event handler
//	userName:                       //User name, may not be required
//	password:                       //Account password, may not be required
};

var ledState=false;

var sensorGauge;        //Google Gauge Object
var sensorGaugeOptions; //Google Gauge Options
var sensorGaugeData;    //Google Gauge Data

var mqttMyTopicPrefix = "atxiotmqttQS";  //Set this to something unique to your

function main() {    
    mqttConnection = new Paho.MQTT.Client(mqttAddress, mqttPort, mqttClientID); //Create or Paho connection object
    mqttConnection.onConnectionLost = onMQTTConnectionLost;                     //Set Connection lost event handler   
    mqttConnection.onMessageArrived = onMQTTMessageArrived;                     //Set Message recive event handler
    mqttConnection.onMessageDelivered = onMQTTMessageDelivered;                 //Set Message sent event handler
    mqttConnection.connect(mqttConnectionOptions);                              //Start the connection

    var buttonElement = document.getElementById("testSpot");                    //Get a reference to our test button html element
	buttonElement.addEventListener("click",onButtonClick);                      //Set the click event handler for the button

    google.charts.load('current', {'packages':['gauge']});                      //Googles library compoent loader
    google.charts.setOnLoadCallback(setupSensorGauge);                          //Set a handler to setup the gauge when the module is loaded
}

function setupSensorGauge() {  //Do the Google gauge setup work
    sensorGauge = new google.visualization.Gauge(document.getElementById('sensorGauge')); //Get a reference to our sensorGauge html element
    sensorGaugeOptions = {  //Create the options object
        width: 400,         //Make the gauge 400px wide
        height: 400,        //Make the gauge 400px tall
        max:4,              //Set the max scale to 4 or 4Volts.  Could set min but zero is default
        minorTicks: 10      //Set 10 ticks between each major division (4)
    };
    sensorGaugeData = google.visualization.arrayToDataTable([   //Create a special google table to store the data
        ['Label', 'Value'],                                     //Gauges lable and Value are collumn 0 and 1
        ['VDC', 0],                                             //The lable is "VDC" the value is 0 for now
    ]);
    sensorGauge.draw(sensorGaugeData, sensorGaugeOptions);      //Draw the gauge
}

function onMQTTConnect(event) { //Handle MQTT connection event
    console.log("onMQTTConnect");                       //Announce the connection
    mqttConnection.subscribe(mqttMyTopicPrefix+"/#");   //Subscribe to all messages with our prefix

	var testMessage=new Paho.MQTT.Message("Hello MQTT World");  //Create a message with it's payload
	testMessage.destinationName=mqttMyTopicPrefix+"/hello";     //Send to the test topic with our prefix
	testMessage.qos=0;                                          //Quality of Service, 0 is don't sweat it, like UDP.
	testMessage.retained=false;                                 //don't need to retain the message
	mqttConnection.send(testMessage);                           //Send our hello message
}

function onMQTTFail(error) {    //Handle MQTT fail event
    console.log("onMQTTFail");      //Announce the failure
    if (error.errorCode !== 0) {    //Display the message if we have one
        console.log("  --" + error.errorMessage);
    }
}

function onMQTTConnectionLost(responseObject) {  //Handle the MQTT disconnect event
    console.log("onMQTTConnectionLost");    //Announce the failure
    if (responseObject.errorCode !== 0) {   //Display the message if we have one
        console.log("  --" + responseObject.errorMessage);
    }
}

function onMQTTMessageArrived(message) {  //Handle a new MQTT receive message
    var payload = message.payloadString;    //Extract payload from message
    var topic = message.destinationName;    //Extract topic from message

    console.log("MQTT RX:"+topic+" : "+payload);    //Anounce

    switch (topic) {    //Switch on the received topic
        case (mqttMyTopicPrefix+"/led") :  //LED message received
            onLEDMessage(payload);  //Call LED message handler
            break;
        case (mqttMyTopicPrefix+"/adc") :  //ADC message recived
            onADCMessage(payload);  //Call ADC message handler
            break;
        default :
            console.log("Unknown MQTT message topic:"+topic);
            break;
    }
}

function onLEDMessage(payload){
    if (payload=="on"){
        elementSetClass("testSpot","LEDOn");        
        ledState=true;
    }
    if (payload=="off"){
        elementSetClass("testSpot","");
        ledState=false;
    }
}

function onADCMessage(payload){
    var sample=parseFloat(payload);
    sensorGaugeData.setValue(0, 1, sample);
    sensorGauge.draw(sensorGaugeData, sensorGaugeOptions);
}

function onMQTTMessageDelivered(message) {
    var payload = message.payloadString;
    var topic = message.destinationName;

    console.log("MQTT TX:"+topic+" : "+payload);
}

function onButtonClick(event){
    if (ledState===false){  //Off now, turn on  
        ledState=true;
        sendLEDCommand("on");                
    }
    else {  //Is on now, turn off
        ledState=false;
        sendLEDCommand("off");                
    }
}

function sendLEDCommand(command){
   	var message=new Paho.MQTT.Message(command); //Create a message with it's payload
    message.destinationName=mqttMyTopicPrefix+"/led"; //Send to the test topic with our prefix
    message.qos=0;  //Quality of Service, 0 is don't sweat it, like UDP.
    message.retained=false;  //don't need to retain the message
    mqttConnection.send(message);
}

//http://stackoverflow.com/questions/195951/change-an-elements-class-with-javascript
function elementSetClass(elementID,classID){
    document.getElementById(elementID).className = classID;
}

window.onload=main;
//main();
