'use strict';
//Simple javascript example for the AustinIoT MQTT QuickStart
//Find more info at the BitBucket Repo
//https://bitbucket.org/uCHobby/web_mqtt_quickstart
//David Fowler

/*global Messaging */  //Tell linter that we have a global named Messaging. 

//MQTT Broker 
var mqttAddress = "austiniot.com"; //**Change This***
var mqttClientID = "uCHobbyWeb"; //**Change This***  Must be unique across all connections, one id per.
var mqttPort = 8883;
var mqttConnection;
var ledState=false;

var mqttConnectionOptions = {
    onSuccess: onMQTTConnect,
	onFailure: onMQTTFail
//	userName:
//	password:
};

var mqttMyTopicPrefix = "atxiotmqttQS";  //Set this to something unique to your

function main() {    
    mqttConnection = new Paho.MQTT.Client(mqttAddress, mqttPort, mqttClientID);
    mqttConnection.onConnectionLost = onMQTTConnectionLost;
    mqttConnection.onMessageArrived = onMQTTMessageArrived;
    mqttConnection.onMessageDelivered = onMQTTMessageDelivered;
    mqttConnection.connect(mqttConnectionOptions);

    var buttonElement = document.getElementById("testSpot");
	buttonElement.addEventListener("click",onButtonClick);
}

function onMQTTConnect(event) {
    console.log("onMQTTConnect");
    mqttConnection.subscribe(mqttMyTopicPrefix+"/#"); //Subscribe to all messages with our prefix

	var testMessage=new Paho.MQTT.Message("Hello MQTT World"); //Create a message with it's payload
	testMessage.destinationName=mqttMyTopicPrefix+"/test"; //Send to the test topic with our prefix
	testMessage.qos=0;  //Quality of Service, 0 is don't sweat it, like UDP.
	testMessage.retained=false;  //don't need to retain the message
	mqttConnection.send(testMessage);
}

function onMQTTFail(error) {
    console.log("onMQTTFail");
    if (error.errorCode !== 0) {
        console.log("  --" + error.errorMessage);
    }
}

function onMQTTConnectionLost(responseObject) {
    console.log("onMQTTConnectionLost");
    if (responseObject.errorCode !== 0) {
        console.log("  --" + responseObject.errorMessage);
    }
}

function onMQTTMessageArrived(message) {
    var payload = message.payloadString;
    var topic = message.destinationName;

    console.log("MQTT RX:"+topic+" : "+payload);

    if (payload=="on"){
        elementSetClass("testSpot","LEDOn");        
        ledState=true;
    }
    if (payload=="off"){
        elementSetClass("testSpot","");
        ledState=false;
    }

}

function onMQTTMessageDelivered(message) {
    var payload = message.payloadString;
    var topic = message.destinationName;

    console.log("MQTT TX:"+topic+" : "+payload);
}

function onButtonClick(event){
    if (ledState===false){  //Off now, turn on  
        ledState=true;
        sendLEDCommand("on");                
    }
    else {  //Is on now, turn off
        ledState=false;
        sendLEDCommand("off");                
    }
}

function sendLEDCommand(command){
   	var message=new Paho.MQTT.Message(command); //Create a message with it's payload
    message.destinationName=mqttMyTopicPrefix+"/led"; //Send to the test topic with our prefix
    message.qos=0;  //Quality of Service, 0 is don't sweat it, like UDP.
    message.retained=false;  //don't need to retain the message
    mqttConnection.send(message);
}

//http://stackoverflow.com/questions/195951/change-an-elements-class-with-javascript
function elementSetClass(elementID,classID){
    document.getElementById(elementID).className = classID;
}

window.onload=main;
//main();
