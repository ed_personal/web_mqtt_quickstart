'use strict';
//Simple javascript example for the AustinIoT MQTT QuickStart
//Find more info at the BitBucket Repo
//https://bitbucket.org/uCHobby/web_mqtt_quickstart
//David Fowler

/*global Messaging */  //Tell linter that we have a global named Messaging. 

//MQTT Broker 
var mqttAddress = "austiniot.com"; //**Change This***
var mqttClientID = "uCHobbyWeb"; //**Change This***  Must be unique across all connections, one id per.
var mqttPort = 8883;
var mqttConnection;

var mqttConnectionOptions = {
    onSuccess: onMQTTConnect,
	onFailure: onMQTTFail
//	userName:
//	password:
};

var mqttMyTopicPrefix = "uCHobby";  //Set this to something unique to your

function main() {    
    mqttConnection = new Paho.MQTT.Client(mqttAddress, mqttPort, mqttClientID);
    mqttConnection.onConnectionLost = onMQTTConnectionLost;
    mqttConnection.onMessageArrived = onMQTTMessageArrived;
    mqttConnection.onMessageDelivered = onMQTTMessageDelivered;
    mqttConnection.connect(mqttConnectionOptions);
}

function onMQTTConnect(event) {
    console.log("onMQTTConnect");
    mqttConnection.subscribe(mqttMyTopicPrefix+"/#"); //Subscribe to all messages with our prefix

	var testMessage=new Paho.MQTT.Message("Hello MQTT World"); //Create a message with it's payload
	testMessage.destinationName=mqttMyTopicPrefix+"/test"; //Send to the test topic with our prefix
	testMessage.qos=0;  //Quality of Service, 0 is don't sweat it, like UDP.
	testMessage.retained=false;  //don't need to retain the message
	mqttConnection.send(testMessage);
}

function onMQTTFail(error) {
    console.log("onMQTTFail");
    if (error.errorCode !== 0) {
        console.log("  --" + error.errorMessage);
    }
}

function onMQTTConnectionLost(responseObject) {
    console.log("onMQTTConnectionLost");
    if (responseObject.errorCode !== 0) {
        console.log("  --" + responseObject.errorMessage);
    }
}

function onMQTTMessageArrived(message) {
    var payload = message.payloadString;
    var topic = message.destinationName;

    console.log("MQTT RX:"+topic+" : "+payload);

}

function onMQTTMessageDelivered(message) {
    var payload = message.payloadString;
    var topic = message.destinationName;

    console.log("MQTT TX:"+topic+" : "+payload);
}

main();
