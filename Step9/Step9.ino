//AustionIoT ESP8266 MQTT Quickstart Step 4: MQTT Hello Chat
//https://bitbucket.org/uCHobby/esp8266_mqtt_quickstart

#include <ESP8266WiFi.h>    //Library for the ESP to work with Arduino 
#include <PubSubClient.h>   //Library for MQTT connections

//IO related constants
const int BLUE_LED=16;    //Blue LED on the NodeMCU module
const int FLASH_BUTTON=0; //Flash button is connected to GPIO
const int ADC_INPUT=A0;	  //ADC Input on ESP8266
const int LED_ON=0;		  //The LED is on when you write 0 to the GPIO
const int LED_OFF=1;	  //LED is off with 1.
const int BUTTON_ON=0;    //Button goes low on press
const int BUTTON_OFF=1;   //Button goes high normally

//Loop polling functions
void connectionUpdate(void);	//Ceck Wifi and MQTT connection status
void buttonUpdate(void);		//Check our button input
void sensorUpdate(void);		//Update the ADC value

//MQTT Functions
void onMQTTMessage(char* topic, byte* payload, unsigned int length);  //Callback function for received MQTT Messages
void mqttSendLEDMessage(bool status);		//Send the MQTT LED Message
void mqttSendADCMessage(float voltage);		//Send the MQTT ADC Message

//Network related event handlers
void onNetConnect(void);		//Handle WiFi connect
void onNetDisconnect(void);		//Handle WiFi disconnect
void onMQTTConnect(void);		//Handle MQTT connect
void onMQTTDisconnect(void);  	//Handle MQTT disconnect

//Application event handlers
void onButtonDown(void);		//User pressed the button
void onButtonUp(void);			//User released the button
void onSensorUpdateTimer(void); //It's time to update the sensor data (ADC)

//Flags used to keep track of connection status
bool netConnected=false;		//Flag for network connection.  WiFi etc...
bool mqttConnected=false;		//Flag for MQTT connection.

//Flags for button and LED states
bool buttonState=false;         //button state flag, pressed = true
bool ledState=false;            //Flag to track status of the LED on or off
unsigned int adcLastValue=0;	//holds the last updated ADC value
unsigned long sensorUpdateTimeMS=0;  //holds the last update time for the ADC value

//**CHANGE THIS**
//Configuration constants
char* mqttID= "uCHobbyDevice";   			//ID used for MQTT connection, must be unique across everything, one per connection  **CHANGE THIS**
char* wifiSSID="iPhone Alfa";				//WiFi SSID.  **CHANGE THIS**
char* wifiPassword="011235813";				//Password for WiFi  **CHANGE THIS**
const char* mqttServer = "austiniot.com";	//Address for the MQTT broker **CHANGE THIS**
const int mqttPort=1883; 					//Port for MQTT connection
String mqttTopicPrefix = "atxiotmqttQS";	//MQTT message prefix, needs to be unique to your application
const int sensorUpdateIntervalMS=100;  		//update sensor data at 100MS rate

WiFiClient espClient;						//Adapter for network services used by PubSubClient

PubSubClient mqttClient( mqttServer,mqttPort,espClient);  //Adapter for MQTT Broker


void setup() {		//Arduino setup function, runs once at reset
	pinMode(BLUE_LED,OUTPUT);			//configure the LED IO pin to output mode
	digitalWrite(BLUE_LED,LED_OFF);		//Start with the LED off

	Serial.begin(115200);				//Set the Serial up for 115200 BPS
	delay(500);							//Wait a bit 
	Serial.println("\r\n\r\n");  		//Print a few blank lines to get past junk
	Serial.println("ESP8266 MQTT Quick Start");  //Say hello over the terminal
	
	//Start WiFi connection
	Serial.print("Connecting to ");
	Serial.println(wifiSSID);	
	WiFi.begin(wifiSSID, wifiPassword);	//Start up the WiFi connection

	//MQTT setup
	mqttClient.setCallback(onMQTTMessage);	//Set the MQTT callback for when we get messages later.
}

void loop() {  			//Arduino loop function, runs over and over...
	connectionUpdate(); //Monitor the web and MQTT connections
	buttonUpdate(); 	//Monitor the pushbutton
    sensorUpdate();		//Monitor the sensor (ADC)
}

//Check the web and MQTT connection status
void connectionUpdate(void){
	//WiFi connection status
	if(WiFi.status()==WL_CONNECTED) {  			//Connected to WiFi?
		if(!netConnected){  					//New WiFi Connection?
			onNetConnect();
		}
		netConnected=true;						//Flag Connected
	}
	else { 										//Not connected to WiFi
		if(netConnected) { 						//Lost connection?
			onNetDisconnect();
		}
		netConnected=false;						//Flag Not Connected
	}

	//MQTT connection status
	if(mqttClient.connected()) {  				//Connected to MQTT
		if(!mqttConnected) {  					//New MQTT Connection?
			onMQTTConnect();
		}
		mqttConnected=true;						//Flag MQTT Connected
	}
	else {  									//Not connected to MQTT
		if (mqttConnected) {  					//Lost MQTT connection?
			onMQTTDisconnect();
		}
		mqttConnected=false;					//Flag MQTT not connected.
	}
	mqttClient.loop();							//Give the MQTT library some processing time CRITICAL!
}

//Monitor the pushbutton
void buttonUpdate(void) { 
	int buttonValue;

	buttonValue=!digitalRead(FLASH_BUTTON);	//use Not as button is low active. 0=active
	if (buttonValue!=buttonState) {  		//Button Changed? 
		if (buttonValue) {  				//if true then button is just now pressed
			onButtonDown();					//Call the button down event handler
		}
		else {								//The change is a button up event
			onButtonUp();					//Call the button up event handler
		}
		buttonState=buttonValue;			//Store the new state to watch for the next change
	}
}

//Monitor the sensor (ADC)
void sensorUpdate(void){
	unsigned int currentTimeMS;				//Store the current time in MS
	unsigned int ellapsedTimeMS;			//Calculated time change in MS

	currentTimeMS=millis();					//Capture the current time in MS
	ellapsedTimeMS=currentTimeMS-sensorUpdateTimeMS;	//Calculate the elappsed time
	if (ellapsedTimeMS>sensorUpdateIntervalMS) {		//Have we waited long enough?
		sensorUpdateTimeMS=currentTimeMS;				//Store the current time mark for future
		onSensorUpdateTimer();							//Call the timer elappsed event handler
	}
}

//WiFi connect event handler
void onNetConnect(void){
	Serial.println("Connected WiFi!");	//Announce success
	Serial.println("MQTT: Connecting"); //Now that we have a netwowrk connection lets get MQTT going
	mqttClient.connect(mqttID);			//Connect to MQTT broker
}

//WiFi disconnect event handler
void onNetDisconnect(void){
	Serial.println("Disconnected WiFi"); //Announce the disconnect
}

//MQTT connect event handler
void onMQTTConnect(void){
	Serial.println("MQTT Connected");			//Announce the connection
	String rxTopic = mqttTopicPrefix+"/#";  	//Build the prefix for our project
	mqttClient.subscribe(rxTopic.c_str());		//Subscribe to all messages with the project prefix
	String txTopic = mqttTopicPrefix+"/hello";	//Build a project hello topic
	mqttClient.publish(txTopic.c_str(), "I'm Alive!");  //Send a hello message.
}

//MQTT disconnect event handler
void onMQTTDisconnect(void){
	Serial.println("MQTT Disconnected");	//Announce the disconnect
}

//MQTT Handler for received messages
void onMQTTMessage(char* topic, byte* payload, unsigned int length) {
	//Print message details
  	Serial.print("Message arrived [");
  	Serial.print(topic);  //topic is a char array so we can print it directly.
  	Serial.print("] ");

    //The payload is a byte buffer that requires special handling to be safe.
	String payloadString("");  //Create a string to hold the payload
   	for (int i = 0; i < length; i++) {  //Walk though the buffer
 	    payloadString+=(char)payload[i]; //convert each byte to a char and append to the payloadString 
   	}
  	Serial.println(payloadString);  //Now we can print the payload string for debugging

	String topicString(topic);	//Create a string from the topic. This makes it easy to do compairisons with
	if (payloadString=="on"){   //Is the payload an on command?
		Serial.println("On Command");	//Announce it
		digitalWrite(BLUE_LED,LED_ON);	//Turn on the LED
	} 
	if (payloadString=="off"){	//Is the payload an off command?
		Serial.println("Off Command");	//Announce it
		digitalWrite(BLUE_LED,LED_OFF); //Turn off the LED
	} 
}

//Button down event handler
void onButtonDown(void){
	Serial.println("Down  ");	//Announce the event

	ledState=!ledState;			//Toggle the LED state flag

	digitalWrite(BLUE_LED,ledState);	//Set the LED to match the flag
	mqttSendLEDMessage(ledState);		//Notify everyone via the MQTT broker
}

//Button up event handler
void onButtonUp(void){
	Serial.println("Up");	//Announce the event
}

//Sensor Timer evet handler
void onSensorUpdateTimer(void){
	int adcRaw;		//Place to store the ADC raw value
	int adcChange;	//Place to stor the value change
	float voltage;  //Calulated ADC input voltage

	adcRaw=analogRead(ADC_INPUT);  	//Get the ADC input reading (0-1024 range)
	voltage=(map(adcRaw,9,998,0,3300))/1000.0;	//Convert the raw value to VDC*1000.0. The map function is int only.
												//Map range values found expermentally. Pot min was 9 and max was 998.
	adcChange=adcLastValue-adcRaw;	//use the raw value to find change.

	if ((adcChange<-4) || (adcChange>4)) {  //is the change large enough to report?
		adcLastValue=adcRaw;	//Save the new ADC base value
		//Report the change
		Serial.print("ADC:"); Serial.print(voltage); Serial.print(" VDC  Change:"); Serial.println(adcChange);

		mqttSendADCMessage(voltage);  //Notify everyone via the MQTT broker of the ADC change
	}	
}

//MQTT Message sender for LED 
void mqttSendLEDMessage(bool status){
	String txTopic = mqttTopicPrefix+"/led";		//Build the LED message MQTT topic

	if(status) { 	//Is LED on? 
		mqttClient.publish(txTopic.c_str(), "on");  //Send the LED message with "on" payload
	}
	else { 			//Is LED off? 
		mqttClient.publish(txTopic.c_str(), "off"); //Send the LED message with "off" payload
	}
}

//MQTT Message sender for ADC
void mqttSendADCMessage(float voltage){
	String txTopic = mqttTopicPrefix+"/adc";		//build the ADC message MQTT topic
	mqttClient.publish(txTopic.c_str(), String(voltage).c_str()); //Send the ADC message
}